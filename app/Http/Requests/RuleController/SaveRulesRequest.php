<?php

namespace App\Http\Requests\RuleController;

use App\Enums\ContainRule;
use App\Enums\ShowRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaveRulesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'rules' => ['required', 'array'],
            'rules.*.show' => ['required', 'string', Rule::in([ShowRule::DONT_SHOW, ShowRule::SHOW])],
            'rules.*.contain' => [
                'required',
                'string',
                Rule::in([
                    ContainRule::PAGE_THAT_CONTAIN,
                    ContainRule::SPECIFIC_PAGE,
                    ContainRule::PAGE_STARTING_WITH,
                    ContainRule::PAGE_ENDING_WITH
                ])
            ],
            'rules.*.url' => ['required', 'string'],
            'message' => ['required', 'string']
        ];
    }
}
