<?php

namespace App\Http\Controllers;

use App\Http\Requests\RuleController\SaveRulesRequest;
use App\Models\Message;
use App\Models\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class RuleController extends Controller
{
    /**
     * Save Computatio Of Rules By A User On The Dashboard.
     *
     * */
    public function store(SaveRulesRequest $request)
    {
        $rules = collect($request->validated()['rules'])->map(function ($rule) {
            return [
                'show' => $rule['show'],
                'contain' => $rule['contain'],
                'user_id' => auth()->id(),
                'url' => $rule['url']
            ];
        });


        // Rule::query()->upsert($rules->toArray(), ['url', 'user_id'], ['show', 'contain']);

        DB::transaction(function () use ($rules, $request) {
            // Delete former Records
            Rule::whereUserId(auth()->id())->delete();

            //Create most updted data
            Rule::query()->insert($rules->toArray());

            // Save Message
            Message::query()->updateOrCreate(['user_id' => auth()->id()], ['title' => $request->message]);
        });

        return Redirect::back();
    }
    /**
     * Endpoint to handle page targeting
     */
    public function handlePageTargeting(Request $request)
    {
        $userId = decrypt($request->id);
        $clientFullUrl = $request->url;

        $path = parse_url($clientFullUrl, PHP_URL_PATH);
        $route = trim($path, '/');

        $rules = Rule::whereUserId($userId)->get()->toArray();

        $message = Message::whereUserId($userId)->first();

        $matched = [];

        foreach ($rules as $rule) {
            if ($isContainRuleMet = (bool)$this->determineIfUserRouteContainDomain($rule, $route)) {
                // dd($isContainRuleMet, $rule['url']);
                if ($rule['show'] === 'show' && $isContainRuleMet) {

                    array_push($matched, true);
                    // break;
                } elseif ($rule['show'] === 'dont_show' && $isContainRuleMet) {

                    array_push($matched, false);
                }
            }
        }

        return response()->json(['matched' => empty($matched) ? false : (in_array(false, $matched) ? false : true), 'message' => $message?->title]);
    }

    /**
     * This Function Helps Determin Whether A User Route Agrees With Certain rules
     */
    private function determineIfUserRouteContainDomain($rule, $userRoute)
    {
        $checkThatAtleastOneConditionIsmet = 0;
        if ($rule['contain'] == 'page_that_contain') {
            str_contains($userRoute, $rule['url']) ? $checkThatAtleastOneConditionIsmet += 1 : $checkThatAtleastOneConditionIsmet;
        }

        if ($rule['contain'] == 'specific_page') {
            $userRoute === $rule['url'] ? $checkThatAtleastOneConditionIsmet += 1 : $checkThatAtleastOneConditionIsmet;
        }

        if ($rule['contain'] == 'pages_starting_with') {
            $len = strlen($rule['url']);
            (substr($userRoute, 0, $len) === $rule['url']) ? $checkThatAtleastOneConditionIsmet += 1 : $checkThatAtleastOneConditionIsmet;
        }

        if ($rule['contain'] == 'pages_ending_with') {
            $len = strlen($rule['url']);
            (substr($userRoute,  -$len) === $rule['url']) ? $checkThatAtleastOneConditionIsmet += 1 : $checkThatAtleastOneConditionIsmet;
        }

        return $checkThatAtleastOneConditionIsmet;
    }
}
