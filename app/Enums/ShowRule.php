<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ShowRule extends Enum
{
    const SHOW = 'show';
    const DONT_SHOW = 'dont_show';
}
