<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ContainRule extends Enum
{
    const PAGE_THAT_CONTAIN = 'page_that_contain';
    const SPECIFIC_PAGE = 'specific_page';
    const PAGE_STARTING_WITH = 'pages_starting_with';
    const PAGE_ENDING_WITH = 'pages_ending_with';
}
