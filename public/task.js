document.addEventListener("DOMContentLoaded", function () {

    const baseUrl = 'https://king-klutch.co';

    const scriptTag = document.querySelector(`script[src^="${baseUrl}/task.js"]`);

    const scriptURL = new URL(scriptTag?.getAttribute('src'));

    const searchParams = scriptURL?.searchParams;

    const userId = searchParams?.get('id');

    const url = `${baseUrl}/api/page-targeting?url=${window.location.href}&id=${userId}`;
    // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set the HTTP method and URL
    xhr.open('GET', url);

    // Set the request headers if needed
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Authorization', 'Bearer your_token_here');
    // xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

    xhr.onload = function () {
        if (xhr.status === 200) {
            const response = JSON.parse(xhr.responseText);
            console.log(response);
            if (response?.matched) {
                alert(response?.message);
            }
        } else {
            alert('Request failed. Status:' + xhr.status);
        }
    };

    xhr.onerror = function () {
        console.error('Network error occurred');
    };

    // Send the request
    xhr.send();
})
