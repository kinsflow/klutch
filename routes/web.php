<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RuleController;
use App\Models\Message;
use App\Models\Rule;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Route::get('/task.js', function () {
    return response()->file(public_path('task.js'));
});
Route::get('page-targeting', [RuleController::class, 'handlePageTargeting']);

Route::get('/dashboard', function () {
    $rules = Rule::whereUserId(auth()->id())->get();
    $message = Message::whereUserId(auth()->id())->first();

    return Inertia::render('Dashboard/Dashboard', [
        'userRules' => $rules,
        'message' => $message,
        'userId' => encrypt($message?->user_id)
    ]);

})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::group(['prefix' => 'rule'], function () {
        Route::post('/', [RuleController::class, 'store'])->name('rule.store');
    });
});

require __DIR__ . '/auth.php';
