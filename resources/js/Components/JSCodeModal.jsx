const JSCodeModal = ({ setShowModal, userId = null }) => {
    return (
        <div className="justify-center w-3/10 md:w-1/3 items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div style={{ width: '30rem' }} className="relative w-3/10 md:w-1/3 my-6">
                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                    {/*header*/}
                    <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                        <h3 className="text-3xl px-4 font-semibold">
                            JS Code snippet
                        </h3>
                        <button
                            className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                            onClick={() => setShowModal(false)}
                        >
                            <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                                ×
                            </span>
                        </button>
                    </div>
                    {/*body*/}
                    <div className="relative p-6 flex-auto">
                        <span style={{overflowWrap: 'break-word'}} className={'break-all'}>
                            <code className="my-4 break-all text-slate-500 text-lg leading-relaxed">
                                {`<script src="${import.meta.env.VITE_APP_URL}/task.js?id=${userId}"></script>`}
                            </code>
                        </span>
                    </div>
                    {/*footer*/}
                    <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                        <button
                            className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                            type="button"
                            onClick={() => setShowModal(false)}
                        > 
                            Close
                        </button>

                    </div>
                </div>
            </div>
        </div>

    )
}


export default JSCodeModal
