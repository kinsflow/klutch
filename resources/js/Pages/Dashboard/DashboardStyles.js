import { keyframes, styled } from 'styled-components';
export const DashboardStyles = styled.div`
    /* color: #a5a9b2; */

    & input,
      select,
      input:focus,
      select:focus{
        border-color: #a5a9b2;
        outline:none;
    }

    & .rule {
        opacity: 1;
        transition: opacity 0.3s ease-in-out;
    }

    & .rule.leave{
        opacity: 0;
    };

    & .remove-button{
        display: flex;
        height: 2.625rem;
    }

    & .text-red{
        color: red
    }

    & .sixty-percent-width{
        width: 60%;
    }
`
const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

export const Input = styled.input`
  opacity: 0;
  animation: ${fadeIn} 0.3s ease-in-out forwards;
`;

export const Select = styled.select`
  opacity: 0;
  animation: ${fadeIn} 0.3s ease-in-out forwards;
`;
