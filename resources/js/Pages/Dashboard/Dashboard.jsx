import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, useForm } from '@inertiajs/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { useEffect, useState } from 'react';
import { Transition } from '@headlessui/react';
import InputError from '@/Components/InputError';
import JSCodeModal from '@/Components/JSCodeModal';
import { DashboardStyles, Input, Select } from './DashboardStyles';

export default function Dashboard({ auth, userRules, message, userId }) {
    const [showModal, setShowModal] = useState(false);

    const { data, setData, post, processing, hasErrors, errors, reset, recentlySuccessful } = useForm(
        {
            rules: [
                {
                    show: '',
                    contain: '',
                    url: ''
                }
            ],

            message: ''
        }
    );

    const handleAddInput = () => {
        setData({
            ...data, rules: [...data?.rules, {
                show: '',
                contain: '',
                url: ''
            }]
        });
    };

    const handleRemoveInput = (index) => {
        if (data?.rules?.length < 2) {
            return false;
        }

        const list = [...data?.rules];

        list.splice(index, 1);

        setData({ ...data, rules: list })
    };

    const handleInputChange = (index, e) => {
        const { name, value } = e.target;
        const list = [...data?.rules];

        list[index][name] = value;
        setData({ ...data, rules: list })
    }

    const showValueLabelPair = [
        {
            value: 'show',
            label: 'Show on'
        },
        {
            value: 'dont_show',
            label: 'Don\'t Show on'
        }
    ];

    const containValueLabelPair = [
        {
            value: 'page_that_contain',
            label: 'Any page that contain'
        },
        {
            value: 'specific_page',
            label: 'A specific page'
        },
        {
            value: 'pages_starting_with',
            label: 'Any page starting with'
        },
        {
            value: 'pages_ending_with',
            label: 'Any page ending with'
        },
    ];

    const handleSubmit = () => {
        post(route('rule.store'));
    }

    useEffect(() => {
        const rules = userRules?.map(rule => ({
            show: rule?.show,
            contain: rule?.contain,
            url: rule?.url
        }))

        setData({ message: message?.title, rules: (rules?.length ? rules : data?.rules) });
    }, [userRules]);

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Page targeting rule</h2>}
        >
            {showModal ? (<JSCodeModal setShowModal={setShowModal} userId={userId} />) : null}
            <Head title="Dashboard" />

            <DashboardStyles>
                <div className="py-12">
                    <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        <button type='button' onClick={() => setShowModal(true)} className="p-2 mb-4  mt-4 bg-red-500 text-white rounded flex items-center justify-between my-4" data-modal-target="JSModal" data-modal-toggle="JSModal">
                            Generate JS code
                        </button>
                        <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                            <div className="container mx-auto">
                                <div className='flex justify-between'>
                                    <button className="p-2 ml-4  mt-4 bg-gray-800 text-white rounded flex items-center justify-between my-4" onClick={handleAddInput}>
                                        Add rule <span className={'px-2'}><FontAwesomeIcon icon={faPlus} /></span>
                                    </button>

                                    <div className="flex items-center pr-4 mt-4">
                                        <span className="text-gray-500"><i>message: </i></span>
                                        <input name='url' type="text"
                                            className="w-full p-2 ml-2 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
                                            onChange={(e) => setData({ ...data, message: e.target.value })}
                                            value={data?.message || ''}
                                            autoComplete="off"
                                        />
                                        {/* <InputError message={errors.message} className="mt-2" /> */}
                                    </div>
                                </div>
                                <div className="mt-8 overflow-x-auto">
                                    <table className="w-full border-collapse">
                                        <thead>
                                            <tr className=''>
                                                <th className="text-left px-4 py-2">Display</th>
                                                <th className="text-left px-4 py-2">Page condition</th>
                                                <th className="text-left px-4 py-2">Route</th>
                                                <th className="text-left px-4 py-2">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            {data?.rules?.map((_, index) => (<tr key={index} className='bg-white shadow rule'>
                                                <td className=" px-4 py-2 w-30">
                                                    <Select
                                                        name='show'
                                                        className="w-full p-2 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
                                                        onChange={(e) => handleInputChange(index, e)}
                                                        value={data?.rules[index]['show'] || 'select'}
                                                    >
                                                        <option value={'select'} disabled>Select</option>
                                                        {showValueLabelPair?.map(show => (
                                                            <option key={show?.value} value={show?.value}>{show?.label}</option>
                                                        ))}
                                                    </Select>
                                                </td>
                                                <td className=" px-4 py-2 w-30">
                                                    <Select
                                                        name='contain'
                                                        className="w-full p-2 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
                                                        onChange={(e) => handleInputChange(index, e)}
                                                        value={data?.rules[index]['contain'] || 'select'}
                                                    >
                                                        <option value={'select'} disabled>Select</option>

                                                        {containValueLabelPair?.map(contain => (
                                                            <option key={contain?.value} value={contain?.value}>{contain?.label}</option>
                                                        ))}
                                                    </Select>
                                                </td>
                                                <td className=" px-4 py-2 w-30">
                                                    <div className="flex items-center">
                                                        {/* <span className="text-gray-500"><i>http://domain.com/</i></span> */}
                                                        <Input name='url' type="text"
                                                            className="w-full p-2 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500"
                                                            onChange={(e) => handleInputChange(index, e)}
                                                            value={data?.rules[index]['url']}
                                                            autoComplete="off"
                                                        />
                                                    </div>
                                                </td>
                                                <td className=" px-4 py-2 flex justify-center w-10 ">
                                                    <button title={'remove rule'} className="py-2 w-full remove-button text-xl bg-red-500 text-white rounded flex items-center justify-center" onClick={() => handleRemoveInput(index)}>

                                                        <span>Remove</span>
                                                        {/* <span><FontAwesomeIcon icon={faTimes} /></span> */}
                                                    </button>
                                                </td>
                                            </tr>))}

                                        </tbody>
                                    </table>
                                </div>
                                <div className="flex items-center">
                                    <button onClick={handleSubmit} className="p-2 ml-4 mb-4 mt-4 bg-gray-800 text-white rounded flex items-center justify-between my-4">
                                        Save rule(s)
                                    </button>
                                    <div>
                                        <Transition
                                            show={recentlySuccessful}
                                            enterFrom="opacity-0"
                                            leaveTo="opacity-0"
                                            className="transition ease-in-out"
                                        >
                                            <p className="text-lg text-gray-600 ml-4">Rule Saved.</p>
                                        </Transition>

                                        <Transition
                                            show={hasErrors}
                                            enterFrom="opacity-0"
                                            leaveTo="opacity-0"
                                            className="transition ease-in-out"
                                        >
                                            <p className="text-lg text-red ml-4">{errors?.message || 'Make sure all fields are filled'}</p>
                                        </Transition>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </DashboardStyles>
        </AuthenticatedLayout>
    );
}
