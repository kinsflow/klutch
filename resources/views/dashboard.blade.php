<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Page Targeting') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                {{-- <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div> --}}

                <div class="container mx-auto">
                    <div class="mt-8 overflow-x-auto">
                        <table class="w-full border-collapse">
                            {{-- <thead>
                                <tr>
                                    <th class="border-b px-4 py-2">First Input</th>
                                    <th class="border-b px-4 py-2">Second Input</th>
                                    <th class="border-b px-4 py-2">Third Input</th>
                                    <th class="border-b px-4 py-2">Actions</th>
                                </tr>
                            </thead> --}}
                            <tbody>

                                <tr>
                                    <td class="border-b px-4 py-2 w-1/5">
                                        <select
                                            class="w-full p-2 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500">
                                            <!-- Options for the first dropdown -->
                                        </select>
                                    </td>
                                    <td class="border-b px-4 py-2 w-1/5">
                                        <select
                                            class="w-full p-2 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500">
                                            <!-- Options for the second dropdown -->
                                        </select>
                                    </td>
                                    <td class="border-b px-4 py-2 w-2/5">
                                        <div class="flex items-center">
                                            <span class="text-gray-500"><i>http://domain.com/</i></span>
                                            <input type="text"
                                                class="w-full p-2 ml-2 rounded-lg shadow-sm focus:outline-none focus:ring-2 focus:ring-blue-500" />
                                        </div>
                                    </td>
                                    <td class="border-b px-4 py-2 flex justify-center">
                                        <button class="p-2 bg-red-500 text-white rounded flex items-center">
                                            <i class="fas fa-times mr-1"></i> <!-- Font Awesome icon -->
                                            <span>Remove</span>
                                          </button>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
